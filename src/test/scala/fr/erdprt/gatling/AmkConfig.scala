package fr.erdprt.gatling

import java.lang.System.getProperties

import com.typesafe.config.{Config, ConfigFactory, ConfigParseOptions}
import net.ceedubs.ficus.Ficus._
import net.ceedubs.ficus.readers.ArbitraryTypeReader._
import net.ceedubs.ficus.readers.EnumerationReader.enumerationValueReader

import scala.concurrent.duration._

object AmkConfig {

    def load(): Settings = {
        buildConfig("amk.configFile").as[RootSettings].amk
    }

    private def buildConfig(configFilenameProperties: String): Config = {
        def mergeConfigs(configs: Config*): Config = configs.fold(ConfigFactory.load())((c1, c2) => c2.withFallback(c1))

        def buildConfigFromProperties: Config = ConfigFactory.parseProperties(getProperties)

        def buildConfigFromFilename(filename: String): Config =
            Option(filename)
                .map(filename => ConfigFactory.parseResources(getClass.getClassLoader, filename, ConfigParseOptions.defaults.setAllowMissing(false)))
                .getOrElse(ConfigFactory.load())

        mergeConfigs(
            buildConfigFromFilename(System.getProperty(configFilenameProperties)),
            buildConfigFromProperties)
    }

    /**
      * Définition du noeud de base pour la configuration.
      * Il permet de définir un nom de prefix pour toutes les variables (ici: amk)
      *
      * @param amk
      */
    case class RootSettings(amk: Settings)

    /**
      */
    case class Settings(baseUrl: String,
                        mode: ModeInjection.Value = ModeInjection.smoke,
                        userPerSecond: Double = 1,
                        ramp: FiniteDuration = 1 minute,
                        duration: FiniteDuration = 1 minute,
                        waitMock1: Int = 1,
                        waitMock2: Int = 1,
                        waitMock3: Int = 1,
                        waitMock4: Int = 1,
                        // Le host de toxiproxy
                        toxiProxyHost: String = "",
                        // Le host de la cible                        
                        toxiProxyTargetHost: String = "",
                        toxiProxyTargetPort: Int = 0,
                        toxiProxyEndPointPath: String = "",
                        toxiProxyEndPointPort: Int = 80,
                        toxiProxyBandwithType: String = "",
                      )

    /**
      * Définition des modes d'injection possible.
      * Les valeurs de l'enum correspondent à la chaîne de caractère à utiliser dans le propriété.
      */
    object ModeInjection extends Enumeration {
        val smoke, constant = Value
    }
    
}
