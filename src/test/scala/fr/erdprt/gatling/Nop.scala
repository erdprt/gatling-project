package fr.erdprt.gatling

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

class Nop extends Simulation {
  
  val config = AmkConfig.load()  
  
  val it = config.productIterator

  println("Test url=						" + config.baseUrl)
  println("Test duration=				" + config.duration)
  println("Test userPerSecond=	" + config.userPerSecond)
  
  val httpProtocolTest  =  http.baseUrl(config.baseUrl)
  val scnTest           =  scenario("Test").exec(http("Test").get("").check(status.is(200)))

  setUp(scnTest.inject(constantUsersPerSec(config.userPerSecond) during(config.duration) ).protocols(httpProtocolTest)) 

}


