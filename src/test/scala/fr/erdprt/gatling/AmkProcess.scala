package fr.erdprt.gatling

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import io.gatling.core.body.CompositeByteArrayBody
import io.gatling.core.check.ValidatorCheckBuilder

// @TODO: 

object AmkProcess {

    def getWithWait(waitTime: Int, mockId: Int) = http("request mock" + mockId + " : wait " + waitTime + "ms")
        .get("http://cortadone.socrate.vsct.fr:58000/api/mock" + mockId + "/simulation/timeout/" + waitTime + "?secret=SUPER_SECURE_PASSWORD")
        .check(status.is(200))

        
    def createProxyScenario(targetHost: String, endPointPort: Int, bandwithRateUpstream: Int, bandwithRateDownstream: Int, latencyValueUpstream: Int, latencyJitterUpstream: Int, latencyValueDownstream: Int, latencyJitterDownstream: Int, targetPort: Int = 80) =   scenario("create_proxy_" + targetHost)
    
                                  .exec( http(targetHost+ "_create").post("")
                                   .body(CompositeByteArrayBody("{\"name\":\"" + targetHost + "\",\"listen\":\"0.0.0.0:" + endPointPort +"\",\"upstream\":\"" + targetHost +":" + targetPort +"\"}"))
                                   .check(status.is(201))
                                  )
                                .exec(http(targetHost + "_bandwidth_upstream").post("/" + targetHost + "/toxics")
                                  .body(CompositeByteArrayBody("{\"attributes\":{\"rate\":" + bandwithRateUpstream + "},\"name\":\"" + targetHost + "_bandwidth_upstream\",\"type\":\"bandwidth\",\"stream\":\"upstream\",\"toxicity\":1}"))
                                  .check(status.is(200))
                                  )
                                .exec(http(targetHost + "_latency_upstream").post("/" + targetHost + "/toxics")
                                  .body(CompositeByteArrayBody("{\"attributes\":{\"latency\":" + latencyValueUpstream + ", \"jitter\":" + latencyJitterUpstream + "},\"name\":\"" + targetHost + "_latency_upstream\",\"type\":\"latency\",\"stream\":\"upstream\",\"toxicity\":1}"))
                                  .check(status.is(200))
                                  )
                                .exec(http(targetHost + "_bandwidth_downstream").post("/" + targetHost +  "/toxics")
                                  .body(CompositeByteArrayBody("{\"attributes\":{\"rate\":" + bandwithRateDownstream + "},\"name\":\"" + targetHost + "_bandwidth_downstream\",\"type\":\"bandwidth\",\"stream\":\"downstream\",\"toxicity\":1}"))
                                  .check(status.is(200))
                                  )
                                .exec(http(targetHost + "_latency_downstream").post("/" + targetHost + "/toxics")
                                  .body(CompositeByteArrayBody("{\"attributes\":{\"latency\":" + latencyValueDownstream + ", \"jitter\":" + latencyJitterDownstream + "},\"name\":\"" + targetHost + "_latency_downstream\",\"type\":\"latency\",\"stream\":\"downstream\",\"toxicity\":1}"))
                                  .check(status.is(200))
                                  )
                                  
   def createProxy(baseUrl: String, targetHost: String, endPointPort: Int, bandwithRateUpstream: Int, bandwithRateDownstream: Int, latencyValueUpstream: Int, latencyJitterUpstream: Int, latencyValueDownstream: Int, latencyJitterDownstream: Int, portTarget: Int = 80) = AmkProcess.createProxyScenario(targetHost, endPointPort, bandwithRateUpstream, bandwithRateDownstream, latencyValueUpstream, latencyJitterUpstream, latencyValueDownstream, latencyJitterDownstream, portTarget)
                                                                                                                          .inject(atOnceUsers(1)).protocols(http
                                                                                                                          .baseUrl(baseUrl)
                                                                                                                          .headers(Map("Content-Type" -> "application/json; charset=utf-8")))                                   
                                  

   def createProxy4G(baseUrl: String, targetHost: String, endPointPort: Int, targetPort: Int = 80) = AmkProcess.createProxyScenario(targetHost, endPointPort, 875, 1625, 45, 36, 45, 36, targetPort)
                                                                                                                          .inject(atOnceUsers(1)).protocols(http
                                                                                                                          .baseUrl(baseUrl)
                                                                                                                          .headers(Map("Content-Type" -> "application/json; charset=utf-8")))                                   
                                                                                                                          

   def createProxy56K(baseUrl: String, targetHost: String, endPointPort: Int, targetPort: Int = 80) = AmkProcess.createProxyScenario(targetHost, endPointPort, 4, 6 , 28 , 22, 28, 22, targetPort)
                                                                                                                          .inject(atOnceUsers(1)).protocols(http
                                                                                                                          .baseUrl(baseUrl)
                                                                                                                          .headers(Map("Content-Type" -> "application/json; charset=utf-8")))                                   

   def createProxyDsl(baseUrl: String, targetHost: String, endPointPort: Int, targetPort: Int = 80) = AmkProcess.createProxyScenario(targetHost, endPointPort, 100, 875 , 28 , 22, 28, 22, targetPort)
                                                                                                                          .inject(atOnceUsers(1)).protocols(http
                                                                                                                          .baseUrl(baseUrl)
                                                                                                                          .headers(Map("Content-Type" -> "application/json; charset=utf-8")))                                   
                                                                                                                          
   def createProxyType(baseUrl: String, targetHost: String, endPointPort: Int, bandwithType: String , targetPort: Int = 80) =   {
      
      bandwithType match {
        case "4G" => createProxy4G(baseUrl, targetHost, endPointPort, targetPort)
        case "56K" => createProxy56K(baseUrl, targetHost, endPointPort, targetPort)
        case "DSL" => createProxyDsl(baseUrl, targetHost, endPointPort, targetPort)
        // @TODO throw exception ?
      }
      
    }                                 
                                                                                                                          
   def deleteProxyScenario(targetHost: String) =   scenario("delete_proxy_" + targetHost)
                                                    .exec( http(targetHost + "_delete").delete("")
                                                    .check(status.is(204))
                                                )
                                                                                                                          
                                                                                                                          
   def deleteProxy(baseUrl: String, targetHost: String) = AmkProcess.deleteProxyScenario(targetHost).
                                                                      inject(
                                                                            nothingFor(2 seconds), atOnceUsers(1)).
                                                                      protocols(http.baseUrl(baseUrl + "/" + targetHost).headers(Map("Content-Type" -> "application/json; charset=utf-8")))
                                                                      
   def getProtocol (port: Int): String = {
     if (port == 443) return "https"
     return "http"
   }                                                                      
                                                                      
}
