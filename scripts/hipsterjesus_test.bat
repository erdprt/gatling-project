SET PATH=C:/WINDOWS/system32;C:/WINDOWS;C:/Windows/System32/WindowsPowerShell/v1.0/;C:/Program Files/Docker/Docker/Resources/bin

SET JAVA_HOME=D:/JAVA/JDK/1.8.0
SET PATH=%PATH%;%JAVA_HOME%/bin

SET M2_HOME=D:/BUILD/MAVEN/apache-maven-3.5.2
SET M2_OPTS=-Xms256m -Xmx768m
SET PATH=%PATH%;%M2_HOME%/bin

SET CURL_HOME=D:\UTILITAIRES\curl
SET PATH=%PATH%;%CURL_HOME%

mvn gatling:test -Dgatling.simulationClass=frontline.sample.DefaultSimulation -Damk.baseUrl="http://localhost:56001/proxies" -Damk.toxiProxyHost="localhost" -Damk.toxiProxyTargetPort=80 -Damk.toxiProxyTargetHost="hipsterjesus.com" -Damk.toxiProxyEndPointPort=65030 -Damk.toxiProxyEndPointPath=/api -Damk.toxiProxyBandwithType="DSL"  -Damk.userPerSecond=1 -Damk.duration="25seconds"